<!DOCTYPE html>
<html>
<head>
    <title>Projet</title>
</head>
<body>
    <h1>Informations du serveur</h1>

    <?php
    // Récupérer le nom d'utilisateur actuel
    $utilisateur = get_current_user();
    echo "<p>Nom d'utilisateur actuel : $utilisateur</p>";

    // Récupérer la distribution utilisée
    $distribution = shell_exec('lsb_release -ds');
    echo "<p>Distribution utilisée : $distribution</p>";

    // Récupérer la version du noyau Linux utilisé
    $versionNoyau = shell_exec('uname -r');
    echo "<p>Version du noyau Linux utilisée : $versionNoyau</p>";

    // Récupérer la taille de la mémoire RAM disponible et totale
    $ramDisponible = shell_exec('free -h | awk \'/^Mem/ {print $7}\'');
    $ramTotale = shell_exec('free -h | awk \'/^Mem/ {print $2}\'');
    echo "<p>Taille de la mémoire RAM disponible : $ramDisponible</p>";
    echo "<p>Taille de la mémoire RAM totale : $ramTotale</p>";

    // Récupérer la taille de stockage disponible et totale
    $stockageDisponible = shell_exec('df -h --output=avail / | awk \'/[0-9]+/ {print $1}\'');
    $stockageTotale = shell_exec('df -h --output=size / | awk \'/[0-9]+/ {print $1}\'');
    echo "<p>Taille de stockage disponible : $stockageDisponible</p>";
    echo "<p>Taille de stockage totale : $stockageTotale</p>";
    ?>
</body>
</html>
